
module PrintLabels
  class Label
    attr_accessor :firstname
    attr_accessor :lastname
    attr_accessor :street
    attr_accessor :zip
    attr_accessor :city
    
    def self.parse(content)
      res = content.map do |dh|
        Label.new(dh)
      end
      if res.size == 1
        res.shift
      else
        res
      end
    end
    
    def initialize(data_hsh)
      @firstname = data_hsh['v']
      @lastname  = data_hsh['n']
      @street    = data_hsh['s']
      @zip       = data_hsh['p']
      @city      = data_hsh['o']
    end
  end
end

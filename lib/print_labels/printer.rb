require 'erb'

module PrintLabels
  class Printer
    attr_accessor :vendor
    attr_accessor :template
    def initialize(vendor)
      @vendor = vendor
    end

    def template
      @template ||= File.read(template_file)
    end
    
    def template_file
      File.join("templates","layout_#{vendor}.tex.erb")
    end
                            
    def print(labels)
      puts render(labels)
    end

    def render(labels)
      labels = labels
      tmpl = ERB.new(template)
      tmpl.result(binding)
    end
  end
end

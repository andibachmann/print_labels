require 'thor'
require 'yaml'

module PrintLabels
  class Cli < Thor
    desc "from <yaml_file>", "print labels as provided by input file"
    def from(file)
      @file = file
      content = YAML.load_file(file)
      labels = Label.parse(content)
      Printer.new('avery-zweckform-3658').print(labels)
    end
  end
end

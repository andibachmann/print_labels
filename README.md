# PrintLabels

Small wrapper for printing labels of yaml-file-based addresses.

## Usage

ERB-TeX-Templates reside in the `templates` subdir. The templates's
filename shoud be named as `layout_#{some_vendor_string}.tex.erb`.

Currently there is one template for Avery-Zweckform ID: 3658 (->
`layout_avery-zweckform-3658.tex.erb`).


